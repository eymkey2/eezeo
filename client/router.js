Accounts.ui.config({
  passwordSignupFields: 'USERNAME_ONLY'
});

Router.configure({
	layoutTemplate: 'layout'
});



// require login
Router.onBeforeAction( function () {
  if ( !Meteor.userId() ) {
    this.redirect( '/landingpage' );
    this.next();
  }
  else {
  	this.subscribe('me');
    this.next();
  }
});

// redirect to dashboard or landingpage
Router.route( '/', function () {

	this.redirect( 'dashboard' );
});

// dashboard
Router.route( '/dashboard', {
	subscriptions: function(){
		return [
			Meteor.subscribe('orders')
		];
	},

	data: function () {
		return {
			orders: new ReactiveVar(Orders.find({'date': moment().format('DDMMYY')})),
			buttons: [
				{href: '/stats', name: 'Stats'},
				{href: '/settings', name: 'Settings'}				
			]
		}
	},

	action: function(){
		this.render('dashboard');
	}
});

// landingpage
Router.route( '/landingpage', function () {
	
	// redirect user to dashboard, if logged in
	if( Meteor.userId() ){
		this.redirect('/');
	}

	this.render( 'landingpage' );
});





// stats
Router.route( '/stats', {
	subscriptions: function(){
		return [
			Meteor.subscribe('orders')
		];
	},

	data: function() {
		return {
			menu: Meteor.user().profile.menu,
			todaysOrders: Orders.find({'date': moment().format('DDMMYY')}),
			allOrders: Orders.find(),
			buttons: [
				{href:'/dashboard', name:'Dashboard'}
			]
		}
	},

	action: function() {
		this.render( 'stats' );
	}
});

// settings
Router.route( '/settings', {
	subscriptions: function(){
		return [
			Meteor.subscribe('me')
		];
	},

	data: function() {
		return {
			collection: Meteor.users,
			doc: Meteor.user(),
			id: 'settingsUpdateForm',
			omit: 'username, fileUrl, customize, services, createdAt',
			buttons: [
				{href:'/customize', name:'Customize'},
				{href:'/dashboard', name:'Dashboard'}
			]
		}
	},

	action: function() {
		this.render( 'settings' );
	}
});

// customize
Router.route( '/customize', {
	subscriptions: function(){
		return [
			Meteor.subscribe('me')
		];
	},

	data: function() {
		return {
			collection: Meteor.users,
			doc: Meteor.user(),
			id: 'updateLogoForm',
			omit: 'username, fileUrl, customize.logo, profile, services, createdAt',
			buttons: [
				{href:'/settings', name:'Settings'}
			]
		}
	},

	action: function() {
		this.render( 'customize' );
	}
});