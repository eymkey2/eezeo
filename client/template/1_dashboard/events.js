Template.dashboard.events({

	'submit #search': function (event, template) {
		event.preventDefault();

		var search = template.find('#search input').value.trim();

		if (search) {
			this.orders.set(Orders.find({'firstName': search,'date': moment().format('DDMMYY')}));
		}
		else {
			this.orders.set(Orders.find({'date': moment().format('DDMMYY')}));
		}
	},

	'click #button': function (event, template) {
		HTTP.call(
			'POST', 
      'http://localhost:3000/updates',
      {
      	data: {
					'update_id': 1234,
					'message': {
						'message_id': 1,
						'from': {
							'id': 47769229,
							'first_name': 'Emre',
							'username': 'eymkey'
						},
						'chat': {
							'id': 47769229,
							'first_name': 'Emre',
							'username':'eymkey',
							'type': 'private'
						},
						'date': 1458951574,
						'text': template.find('#input').value
					}
      	}
     	},
      function (error, result) {
      	
    	}
    );
	}

});