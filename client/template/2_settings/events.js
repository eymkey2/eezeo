Template.settings.events({

	'change #fileupload input': function (event, template) {
		FS.Utility.eachFile(event, function (file) {
      Files.insert(file, function (error, file) {
        if (error){
           console.log(error);
        }
        else {
          var url = file.url({brokenIsFine: true}).split('?')[0];
          Meteor.call('setOrganisationData', 'fileUrl', url);
        }
      });
    });
	}

});