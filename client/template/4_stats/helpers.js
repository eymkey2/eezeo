Template.stats.helpers({
	
	'findTodaysMenu': function (menu) {
		return Orders.find({'menu': menu, 'date': moment().format('DDMMYY')}).count();
	},

	'findAllMenu': function (menu) {
		return Orders.find({'menu': menu}).count();
	},

});