Meteor.methods({



	/*--------------------*\

			CUSTOMER METHODS

	\*--------------------*/

	join: function (customer) {
		customer.balance = 0;
		check(customer, Schemas.customer);

		if (Customers.findOne({'telegramId': customer.telegramId})) {
			throw new Meteor.Error(Errors['already joined']);
		}
		else {
			var organisation = Meteor.call('getOrganisationByName', customer.organisation);
			Customers.insert(customer);
		}
	},

	isJoined: function (telegramId) {
		check(telegramId, Number);

		var customer = Customers.findOne({'telegramId': telegramId});

		if (customer) {
			return customer;
		}
		else {
			throw new Meteor.Error(Errors['not joined']);
		}
	},

	setCustomerData: function (telegramId, property, value) {
		check(telegramId, Number);
		check(property, String);
		check(value, String);

		Meteor.call('isJoined', telegramId);

		switch (property) {
			case 'firstname':
				Customers.update({'telegramId': telegramId}, {$set: {'firstName': value}}, cb);
				break;

			case 'lastname':
				Customers.update({'telegramId': telegramId}, {$set: {'lastName': value}}, cb);
				break;

			case 'organisation':
				Meteor.call('getOrganisationByName', value);
				Customers.update({'telegramId': telegramId}, {$set: {'organisation': value}}, cb);
				break;

			default:
				throw new Meteor.Error('property' + Errors['not supported']);
		}

		function cb (error, result) {
			if (error) {
				throw new Meteor.Error(Errors['failed']);
			}
		}
	},





	/*------------------------*\

			ORGANISATION METHODS

	\*------------------------*/

	getOrganisationByName: function (name) {
		check(name, String);

		var user = Meteor.users.findOne({'username': name});

		if (user) {
			return user;
		}
		else {
			throw new Meteor.Error('organisation' + Errors['not exists']);
		}
	},

	setOrganisationData: function (property, value) {
		check(property, String);
		check(value, String);

		switch (property) {
			case 'logo':
				var rootUrl = process.env.ROOT_URL;
				var lastChar = rootUrl[rootUrl.length - 1];
				var url;

				if(lastChar === '/'){
					url = rootUrl + value.substring(1, value.length);
				}
				else {
					url = rootUrl + '/' + value.substring(1, value.length);
				}

		    Meteor.users.update({'_id': this.userId}, {$set: {'customize.logo': url}}, cb);
				break;

			case 'fileUrl':
				var rootUrl = process.env.ROOT_URL;
				var lastChar = rootUrl[rootUrl.length - 1];
				var url;

				if(lastChar === '/'){
					url = rootUrl + value.substring(1, value.length);
				}
				else {
					url = rootUrl + '/' + value.substring(1, value.length);
				}
				
		    Meteor.users.update({'_id': this.userId}, {$set: {'fileUrl': url}}, cb);
				break;

			case 'lastOrderableTime':
				Meteor.users.update({'_id': this.userId}, {$set: {'profile.lastOrderableTime': value}}, cb);
				break;

			case 'lastCancellableTime':
				Meteor.users.update({'_id': this.userId}, {$set: {'profile.lastCancellableTime': value}}, cb);
				break;

			default:
				throw new Meteor.Error('property ' + Errors['not supported']);
		}

		function cb (error) {
    	if (error) {
    		throw new Meteor.Error(Errors['failed']);
    	}
    }
	},

	updateProfile: function (doc) {
		check(doc.profile, Schemas.profile);
		
		Meteor.users.update({'_id': this.userId}, {$set: {'profile': doc.profile}}, function (error) {
    	if (error) {
    		throw new Meteor.Error(Errors['failed']);
    	}
    });
	},

	updateLogo: function (doc) {
		check(doc.customize, Schemas.customize);
		doc.customize.logo = Meteor.user().customize.logo;
		
		Meteor.users.update({'_id': this.userId}, {$set: {'customize': doc.customize}}, function (error) {
    	if (error) {
    		console.log(error);
    		throw new Meteor.Error(Errors['failed']);
    	}
    });
	},





	/*-----------------*\

			ORDER METHODS

	\*-----------------*/

	insertOrder: function (telegramId, menu) {
		check(telegramId, Number);
		check(menu, String);

		var customer = Meteor.call('isJoined', telegramId);
		var organisation = Meteor.call('getOrganisationByName', customer.organisation);

		var now = moment();
		var lastOrderableTime = moment(leftpad(organisation.profile.lastOrderableTime.hours, 2, 0) + '' + leftpad(organisation.profile.lastOrderableTime.minutes, 2, 0), 'HHmm');

		if (now.isBefore(lastOrderableTime)) {

			for (var i = 0; i < organisation.profile.menu.length; i++) {
				if (organisation.profile.menu[i].name === menu) {

					var order = Orders.findOne({'telegramId': customer.telegramId, 'organisation': organisation._id, 'date': now.format('DDMMYY')});
					var allOrders = Orders.find({'organisation': organisation._id, 'date': now.format('DDMMYY')}).count();

					if (order) {
						throw new Meteor.Error(Errors['already ordered']);
					}
					else if (allOrders >= organisation.profile.menu[i].amount) {
						throw new Meteor.Error(Errors['limit reached'] + organisation.profile.menu[i].amount);
					}
					else {
						Orders.insert({
							telegramId: customer.telegramId,
							firstName: customer.firstName,
							lastName: customer.lastName,

							menu: menu,
							date: now.format('DDMMYY'),
							organisation: organisation._id,
							createdAt: new Date()
						});
					}

					return;
				}
			}

			throw new Meteor.Error('menu' + Errors['not exists']);
		}
		else {
			var message = Errors['cant order'] + 'after ' + leftpad(organisation.profile.lastOrderableTime.hours, 2, 0) + ':' + leftpad(organisation.profile.lastOrderableTime.minutes, 2, 0);
			throw new Meteor.Error(message);
		}
	},

	getOrder: function (telegramId) {
		check(telegramId, Number);

		var customer = Meteor.call('isJoined', telegramId);
		var organisation = Meteor.call('getOrganisationByName', customer.organisation);
		
		var order = Orders.findOne({'telegramId': customer.telegramId, 'organisation': organisation._id, 'date': moment().format('DDMMYY')});

		if (order) {
			return order;
		}
		else {
			throw new Meteor.Error(Errors['not ordered']);
		}
	},

	removeOrder: function (telegramId) {
		check(telegramId, Number);

		var customer = Meteor.call('isJoined', telegramId);
		var organisation = Meteor.call('getOrganisationByName', customer.organisation);

		var now = moment();
		var lastCancellableTime = moment(leftpad(organisation.profile.lastCancellableTime.hours, 2, 0) + '' + leftpad(organisation.profile.lastCancellableTime.minutes, 2, 0), 'HHmm');

		if (now.isBefore(lastCancellableTime)) {
			var order = Meteor.call('getOrder', telegramId);

			Orders.remove({'_id': order._id}, function (error){
				if (error) {
					throw new Meteor.Error(Errors['failed']);
				}
			});
		}
		else {
			var message = Errors['cant cancel'] + 'after ' + leftpad(organisation.profile.lastCancellableTime.hours, 2, 0) + ':' + leftpad(organisation.profile.lastCancellableTime.minutes, 2, 0);
			throw new Meteor.Error(message);
		}
	},





	/*----------------*\

			BOT METHODS

	\*----------------*/

	getPort: function(){

		console.log(process.env);
		console.log(process.env.PORT);

	},

	sendMessage: function (id, text, mode) {
		check(id, Number);
		check(text, String);
		check(mode, Match.OneOf(String, undefined));

		bot.sendMessage({
      chat_id: id,
      text: text,
      parse_mode: mode || '',
      cb: function (error, result) {
      	if (error) {
      		console.log('Could not send message to ' + id);
      	}
      }
		});
	}

	//
	//	TODO
	//
	/*sendDocument: function (id, docId) {

		var file = Files.findOne();
		console.log(file.createReadStream('files'));

		bot.sendDocument({
      chat_id: id,
      document: new InputFile(file.createReadStream('files')),
      cb: function(error, result){
      	if(error){
      		console.log(error);
      	}
      	else{
      		console.log(result);
      	}
      }
		});
		
	}*/

});