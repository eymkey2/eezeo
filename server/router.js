Router.route('/updates', function () {
  
  var req = this.request;
  var res = this.response;

  var keywords = ['/join', '/set', '/show', '/order', '/cancel'];

  // /join VORNAME NACHNAME @ORGANISATION
  
  // /set firstname
  // /set lastname
  // /set organisation
  
  // /show me
  // /show menu
  // /show order
  
  // /order MENU
  // /cancel

  var message = req.body.message || {};

  console.log();
  console.log('--------------------');
  console.log('Date: ', moment().format('DD.MM.YY HH:mm'));
  console.log(JSON.stringify(message, null, 2));
  
  if (!message.text) {
    res.end()
    return;
  }

  if (!message.from || !message.chat) {
    res.end()
    return;
  }
  
  message.text = message.text || '';
  message.text = message.text.trim();
  
  var parameters = message.text.split(' ');
  parameters[0] = parameters[0].toLowerCase();

  var chatId = Number(message.chat.id);
  var telegramId = Number(message.from.id);


  if (parameters[0] === '/start' || parameters[0] === 'hi') {
    Meteor.call('sendMessage', chatId, 'Welcome! Type "/help" to see all commands.');
  }

  if (parameters[0] === '/help' || parameters[0] === 'help') {
    Meteor.call('sendMessage', chatId, '/join VORNAME NACHNAME @KANTINE\n\n/set firstname VORNAME\n/set lastname NACHNAME\n/set organisation KANTINE\n\n/show me\n/show menu\n/show order\n\n/order MENU\n/cancel')
  }



  /*-------------*\

      0: JOIN

  \*-------------*/

  if (parameters[0] === keywords[0] || parameters[0] === keywords[0].substring(1, keywords[0].length)) {
    if (parameters.length < 4){
      Meteor.call('sendMessage', chatId, Errors['missing parameters']);
      res.end();
      return;
    }

    var customer = {
      telegramId: telegramId,
      firstName: parameters[1], 
      lastName: parameters[2],
      organisation: parameters[3].substring(1, parameters[3].length)
    }

    Meteor.call('join', customer, function (error) {
      if (error) {
        Meteor.call('sendMessage', chatId, error.error);
      }
      else {
        Meteor.call('sendMessage', chatId, 'congratulation successfully joined!');
      }
    });
  }



  /*-----------*\

      1: SET

  \*-----------*/

  else if (parameters[0] === keywords[1] || parameters[0] === keywords[1].substring(1, keywords[1].length)) {
    if (parameters.length < 3){
      Meteor.call('sendMessage', chatId, Errors['missing parameters']);
      res.end()
      return;
    }

    parameters[1] = parameters[1].toLowerCase();

    switch (parameters[1]) {
      case 'firstname':
      case 'lastname':
      case 'organisation':
        Meteor.call('setCustomerData', telegramId, parameters[1], parameters[2], function (error) {
          if (error) {
            Meteor.call('sendMessage', chatId, error.error);
          }
          else {
            Meteor.call('sendMessage', chatId, 'successfully updated!');
          }
        });
        break;
      default:
        Meteor.call('sendMessage', chatId, 'second parameter' + Errors['not supported']);
    }
  }



  /*----------------*\

      2: SHOW

  \*----------------*/

  else if (parameters[0] === keywords[2] || parameters[0] === keywords[2].substring(1, keywords[2].length)) {
    if (parameters.length < 2){
      Meteor.call('sendMessage', chatId, Errors['missing parameters']);
      res.end()
      return;
    }

    parameters[1] = parameters[1].toLowerCase();

    Meteor.call('isJoined', telegramId, function (error, customer) {
      if (error) {
        Meteor.call('sendMessage', chatId, error.error);
      }
      else {
        var organisation = Meteor.call('getOrganisationByName', customer.organisation);

        switch (parameters[1]) {
          case 'me':
            Meteor.call('sendMessage', chatId, 'firstname: <b>'+customer.firstName+'</b>. lastname: <b>'+customer.lastName+'</b>. organisation: <b>'+customer.organisation+'</b>', 'HTML');
            break;

          case 'menu':
            var url = organisation.fileUrl || '';
            Meteor.call('sendMessage', chatId, '<a href="'+url+'">'+(url ? 'menu' : 'no menu set yet')+'</a>', 'HTML');
            break;

          case 'order':
            Meteor.call('getOrder', customer.telegramId, function (error, order) {
              if (error) {
                Meteor.call('sendMessage', chatId, error.error);
              }
              else {
                Meteor.call('sendMessage', chatId, 'ordered nr ' + order.menu + ' @' + customer.organisation);
              }
            });
            break;

          case 'time':
            Meteor.call('sendMessage', chatId, moment().format('DD.MM.YYYY HH:mm'));
            break;

          default:
            Meteor.call('sendMessage', chatId, 'second parameter' + Errors['not supported']);
        }
      }
    });
  }



  /*-------------*\

      3: ORDER

  \*-------------*/

  else if (parameters[0] === keywords[3] || parameters[0] === keywords[3].substring(1, keywords[3].length)) {
    if (parameters.length < 2){
      Meteor.call('sendMessage', chatId, Errors['missing parameters']);
      res.end()
      return;
    }

    parameters[1] = parameters[1].toLowerCase();

    Meteor.call('insertOrder', telegramId, parameters[1], function (error, order) {
      if (error) {
        Meteor.call('sendMessage', chatId, error.error);
      }
      else {
        Meteor.call('sendMessage', chatId, 'successfully ordered!');
      }
    });
  }
  


  /*--------------*\

      4: CANCEL

  \*--------------*/
  
  else if (parameters[0] === keywords[4] || parameters[0] === keywords[4].substring(1, keywords[4].length)) {
    if (parameters.length < 1){
      Meteor.call('sendMessage', chatId, Errors['missing parameters']);
      res.end()
      return;
    }

    Meteor.call('removeOrder', telegramId, function (error) {
      if (error) {
        Meteor.call('sendMessage', chatId, error.error);
      }
      else {
        Meteor.call('sendMessage', chatId, 'successfully canceled your order!');
      }
    });
  }



  res.end();
}, {where: 'server'});