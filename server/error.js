Errors = {
	'not joined': 'not joined yet, please join first!',
	'already joined': 'already joined!',
	
	'already ordered': 'already ordered!',

	'not exists': ' does not exist!',
	'not ordered': 'you have not ordered yet!',

	'limit reached': 'can not order, limit reached: ',
	'cant cancel': 'can not cancel ',
	'cant order': 'can not order ',

	'failed': 'failed, please try again!',
	'not supported': ' is not supported!',


	'missing parameters': 'missing parameters!'
};