/*---------------*\

		collections

\*---------------*/

Customers = new Mongo.Collection('customers');
Orders = new Mongo.Collection('orders');
Files = new FS.Collection('files', {

 stores: [new FS.Store.GridFS('files')]});


Meteor.users.allow({
	insert: function(){return false},
  update: function(){return false},
  remove: function(){return false}
});
Customers.allow({
	insert: function(){return false},
  update: function(){return false},
  remove: function(){return false}
});
Orders.allow({
	insert: function(){return false},
	update: function(){return false},
	remove: function(){return false}
});
Files.allow({
	insert: function(){return true},
	update: function(){return true},
	remove: function(){return false},
	download: function(){return true}
});





/*------------*\

		Schemas

\*------------*/

Schemas = {};
SimpleSchema.debug = true;


// user
// TODO: make menu name case insensitiv!
Schemas.user = new SimpleSchema({
		'username': {
			type: String,
			regEx: /^[a-zA-Z0-9]+$/,
			min: 4,
			max: 24,
      denyUpdate: true
		},

		'fileUrl': {
			type: String,
			optional: true
		},

		'customize': {
			type: Object,
			optional: true
		},
		'customize.title': {
			type: String
		},
		'customize.logo': {
			type: String,
			optional: true
		},

    'profile': {
	    type: Object,
	    optional: true
    },
		'profile.menu': {
			type: [Object]
		},
		'profile.menu.$.name': {
			type: String
		},
		'profile.menu.$.amount': {
			type: Number
		},

		'profile.lastOrderableTime': {
			type: Object
		},
		'profile.lastOrderableTime.hours': {
			type: Number,
			min: 0,
			max: 23
		},
		'profile.lastOrderableTime.minutes': {
			type: Number,
			min: 0,
			max: 59
		},

		'profile.lastCancellableTime': {
			type: Object
		},
		'profile.lastCancellableTime.hours': {
			type: Number,
			min: 0,
			max: 23
		},
		'profile.lastCancellableTime.minutes': {
			type: Number,
			min: 0,
			max: 59
		},

    'services': {
    	type: Object,
    	blackbox: true
    },
    'createdAt': {
     	type: Date,
     	denyUpdate: true
    }
});

Schemas.profile = new SimpleSchema({
	'menu': {
		type: [Object],
		optional: true
	},
	'menu.$.name': {
		type: String
	},
	'menu.$.amount': {
		type: Number
	},

	'lastOrderableTime': {
		type: Object,
		optional: true
	},
	'lastOrderableTime.hours': {
		type: Number,
		min: 0,
		max: 23
	},
	'lastOrderableTime.minutes': {
		type: Number,
		min: 0,
		max: 59
	},

	'lastCancellableTime': {
		type: Object,
		optional: true
	},
	'lastCancellableTime.hours': {
		type: Number,
		min: 0,
		max: 23
	},
	'lastCancellableTime.minutes': {
		type: Number,
		min: 0,
		max: 59
	},
});

Schemas.customize = new SimpleSchema({
	'title': {
		type: String
	}
});


// customer
Schemas.customer = new SimpleSchema({
	'telegramId': {
		type: Number
	},
	'firstName': {
		type: String
	},
	'lastName': {
		type: String
	},
	'organisation': {
		type: String
	},
	'balance': {
		type: Number
	}
});

//order
Schemas.order = new SimpleSchema({
	'telegramId': {
		type: Number
	},
	'firstName': {
		type: String
	},
	'lastName': {
		type: String
	},
	organisation: {
		type: String,
		regEx: SimpleSchema.RegEx.Id,
    denyUpdate: true
	},
	'menu': {
		type: String,
		denyUpdate: true
	},
	'date': {
		type: String
	},
	'createdAt': {
		type: Date
	}
});





/*-----------------*\

		attach Schema

\*-----------------*/

Meteor.users.attachSchema(Schemas.user);
Orders.attachSchema(Schemas.order);
Customers.attachSchema(Schemas.customer);